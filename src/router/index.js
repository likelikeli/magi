import Vue from 'vue'
import Router from 'vue-router'
import Search from '@/components/index'
import Chart from '@/components/test/chart'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'search',
      component: Search
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/chart',
      name: 'chart',
      component: Chart
    }
  ]
})
